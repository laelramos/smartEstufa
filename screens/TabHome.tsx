import React, { useState, useEffect } from 'react';
import { StyleSheet, Switch } from 'react-native';

import { Text, View } from '../components/Themed';
import api from '../service/api';

export default function TabOneScreen() {
  const [isEnabled, setIsEnabled] = useState(false);
  const [iluminacao, setIluminacao] = useState(false);
  const [temperatura, setTemperatura] = useState("--");
  const [humidade, setHumidade] = useState("--");
  const toggleSwitch = async () => {
    setIsEnabled(previousState => !previousState);
    if (iluminacao == false) {
      const response = await api.get('/on');
      console.log('ligou');
    } else {
      console.log('desligou');
      const response = await api.get('/off');
    }
    atualizaStatus();
  }

  async function atualizaStatus() {
    const response = await api.get('/status');
    setTemperatura(response.data.temperatura);
    setHumidade(response.data.humidade);
    setIluminacao(response.data.iluminacao == 'true');
    console.log("Atualizou");
  }

  useEffect(() => {
    const interval = setInterval(atualizaStatus, 1000);
  }, []);

  useEffect(() => {
    if (iluminacao == false) {
      setIsEnabled(false);
    } else {
      setIsEnabled(true);
    }
  }, [iluminacao]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Temperatura: {temperatura} ºC</Text>
      <Text style={styles.title}>Ar: {humidade} %</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      {/* <EditScreenInfo path="/screens/TabOneScreen.tsx" /> */}
      <Text style={styles.title}>Iluminacao</Text>
      <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={isEnabled ? "#81b0ff" : "#f4f3f4"}
        ios_backgroundColor="#fff"
        onValueChange={toggleSwitch}
        value={iluminacao}
      />
{/* 
      <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={isEnabled ? "#81b0ff" : "#f4f3f4"}
        ios_backgroundColor="#fff"
        onValueChange={toggleSwitch}
        value={iluminacao}
      /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
