import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          TabOne: {
            screens: {
              TabHome: 'one',
            },
          },
          TabTwo: {
            screens: {
              TabSettings: 'two',
            },
          },
          TabThree: {
            screens: {
              TabHome: 'three',
            },
          },
          TaFour: {
            screens: {
              TabHome: 'four',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
