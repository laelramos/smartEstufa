import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import TabHome from '../screens/TabHome';
import TabTimer from '../screens/TabTimer';
import TabProfile from '../screens/TabProfile';
import TabSettings from '../screens/TabSettings';
import { BottomTabParamList, TabOneParamList, TabTwoParamList, TabFourParamList, TabThreeParamList } from '../types';

import { Entypo } from '@expo/vector-icons';
const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabOne"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="Home"
        component={TabOneNavigator}
        options={{
          tabBarIcon: ({ color }) => <Entypo name="home" size={24} color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Timer"
        component={TabTwoNavigator}
        options={{
          tabBarIcon: ({ color }) => <Entypo name="clock" size={24} color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={TabThreeNavigator}
        options={{
          tabBarIcon: ({ color }) => <Entypo name="user" size={24} color={color} />,
        }}
        />
        <BottomTab.Screen
        name="Settings"
        component={TabFourNavigator}
        options={{
          tabBarIcon: ({ color }) => <Ionicons name="md-settings" size={26} color={color} />
        }}
        />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="TabHome"
        component={TabHome}
        options={{ headerTitle: 'Smart Garden' }}
      />
    </TabOneStack.Navigator>
  );
}


const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTimer"
        component={TabTimer}
        options={{ headerTitle: 'Smart Garden' }}
      />
    </TabTwoStack.Navigator>
  );
}


const TabThreeStack = createStackNavigator<TabThreeParamList>();

function TabThreeNavigator() {
  return (
    <TabThreeStack.Navigator>
      <TabOneStack.Screen
        name="TabProfile"
        component={TabProfile}
        options={{ headerTitle: 'Smart Garden' }}
      />
    </TabThreeStack.Navigator>
  );
}


const TabFourStack = createStackNavigator<TabFourParamList>();

function TabFourNavigator() {
  return (
    <TabFourStack.Navigator>
      <TabFourStack.Screen
        name="TabSettings"
        component={TabSettings}
        options={{ headerTitle: 'Smart Garden' }}
      />
    </TabFourStack.Navigator>
  );
}